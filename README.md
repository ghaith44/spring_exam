# Entities Relations

## One To One Unidirectionnelle

```java
@Entity
@Table(name = "T_PARENT")
public class Parent implements Serializable {
private static final long serialVersionUID = 1L;

@Id
@GeneratedValue (strategy = GenerationType.IDENTITY)
@Column(name="PARENT_ID")
private int id;
}

@Entity
@Table(name = "T_CHILD")
public class Child implements Serializable {
private static final long serialVersionUID = 1L;

@Id
@GeneratedValue (strategy = GenerationType.IDENTITY)
@Column(name="CHILD_ID")
private int id;

@OneToOne
private Parent parent;
}
```

## One To One Bidirectionnelle

```java
@Entity
@Table(name = "T_PARENT")
public class Parent implements Serializable {
private static final long serialVersionUID = 1L;

@Id
@GeneratedValue (strategy = GenerationType.IDENTITY)
@Column(name="PARENT_ID")
private int id;

@OneToOne(mappedBy="parent")
private Child child;
}

@Entity
@Table(name = "T_CHILD")
public class Child implements Serializable {
private static final long serialVersionUID = 1L;

@Id
@GeneratedValue (strategy = GenerationType.IDENTITY)
@Column(name="CHILD_ID")
private int id;

@OneToOne
private Parent parent;
}
```

## One To Many Unidirectionnelle

```java
@Entity
@Table(name = "T_PARENT")
public class Parent implements Serializable {
private static final long serialVersionUID = 1L;

@Id
@GeneratedValue (strategy = GenerationType.IDENTITY)
@Column(name="PARENT_ID")
private int id;

@OneToMany(cascade = CascadeType.ALL)
private Set<Child> children;
}

@Entity
@Table(name = "T_CHILD")
public class Child implements Serializable {
private static final long serialVersionUID = 1L;

@Id
@GeneratedValue (strategy = GenerationType.IDENTITY)
@Column(name="CHILD_ID")
private int id;


}
```

## Many To One Unidirectionnelle

```java
@Entity
@Table(name = "T_PARENT")
public class Parent implements Serializable {
private static final long serialVersionUID = 1L;

@Id
@GeneratedValue (strategy = GenerationType.IDENTITY)
@Column(name="PARENT_ID")
private int id;

}

@Entity
@Table(name = "T_CHILD")
public class Child implements Serializable {
private static final long serialVersionUID = 1L;

@Id
@GeneratedValue (strategy = GenerationType.IDENTITY)
@Column(name="CHILD_ID")
private int id;

@ManyToOne(cascade = CascadeType.ALL)
private Parent parents;


}
```

## One To Many Bidirectionnelle = Many To One Bidirectionnelle

```java
@Entity
@Table(name = "T_PARENT")
public class Parent implements Serializable {
private static final long serialVersionUID = 1L;

@Id
@GeneratedValue (strategy = GenerationType.IDENTITY)
@Column(name="PARENT_ID")
private int id;

@OneToMany(cascade = CascadeType.ALL, mappedBy="parents")
private Set<Child> children;
}

@Entity
@Table(name = "T_CHILD")
public class Child implements Serializable {
private static final long serialVersionUID = 1L;

@Id
@GeneratedValue (strategy = GenerationType.IDENTITY)
@Column(name="CHILD_ID")
private int id;

@ManyToOne
private Parent parents;

}
```

## Many To Many Unidirectionnelle

```java
@Entity
@Table(name = "T_PARENT")
public class Parent implements Serializable {
private static final long serialVersionUID = 1L;

@Id
@GeneratedValue (strategy = GenerationType.IDENTITY)
@Column(name="PARENT_ID")
private int id;

@ManyToMany(cascade = CascadeType.ALL)
private Set<Child> children;
}

@Entity
@Table(name = "T_CHILD")
public class Child implements Serializable {
private static final long serialVersionUID = 1L;

@Id
@GeneratedValue (strategy = GenerationType.IDENTITY)
@Column(name="CHILD_ID")
private int id;

}
```

## Many To Many Bidirectionnelle

```java
@Entity
@Table(name = "T_PARENT")
public class Parent implements Serializable {
private static final long serialVersionUID = 1L;

@Id
@GeneratedValue (strategy = GenerationType.IDENTITY)
@Column(name="PARENT_ID")
private int id;

@ManyToMany(cascade = CascadeType.ALL)
private Set<Child> children;
}

@Entity
@Table(name = "T_CHILD")
public class Child implements Serializable {
private static final long serialVersionUID = 1L;

@Id
@GeneratedValue (strategy = GenerationType.IDENTITY)
@Column(name="CHILD_ID")
private int id;

@ManyToMany( cascade = CascadeType.ALL, mappedBy="children")
private Set<Parent> parents;

}
```

## FETCH CASCADE

```java
@OneToMany(mappedBy="entreprise",
cascade = {CascadeType.PERSIST, CascadeType.REMOVE},
fetch=FetchType.EAGER)
private List<Departement> departements = new ArrayList<>();
```
